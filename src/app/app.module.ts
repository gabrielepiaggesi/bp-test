import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { NgxChartsModule } from '@swimlane/ngx-charts';

import { AppComponent } from './app.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { JsonBoxComponent } from './json-box/json-box.component';
import { LoaderBoxComponent } from './loader-box/loader-box.component';
import { ChartBoxComponent } from './chart-box/chart-box.component';

@NgModule({
  declarations: [
    AppComponent,
    JsonBoxComponent,
    LoaderBoxComponent,
    ChartBoxComponent
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    FormsModule,
    NgxChartsModule,
    BrowserAnimationsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
