import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { jsonValidator } from '../json.validator';
import { DataService } from '../data.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-json-box',
  templateUrl: './json-box.component.html',
  styleUrls: ['./json-box.component.scss']
})
export class JsonBoxComponent implements OnInit {
  public jsonGroup: FormGroup;

  constructor(private dataService: DataService) { }

  ngOnInit(): void {
    this.jsonGroup = new FormGroup({
      jsonInput: new FormControl("", [Validators.required, jsonValidator()])
    });
  }

  public loadJson() {
    const inputValue = this.jsonGroup.get('jsonInput').value;
    const json = JSON.parse(inputValue);
    this.dataService.setJson(json);
    var pretty = JSON.stringify(json, undefined, 4);
    (<HTMLInputElement>document.getElementById('myTextArea')).value = pretty;
  }

  public jsonInputIsInvalid() {
    return this.jsonGroup.controls['jsonInput'].invalid && 
    (this.jsonGroup.controls['jsonInput'].dirty || this.jsonGroup.controls['jsonInput'].touched);
  }

  public getJsonInputError() {
    const errors = this.jsonGroup.controls['jsonInput'].errors;
    if (errors.required) return 'isEmpty';
    if (errors.isNotJson) return 'isNotJson';
    if (errors.isNotArray) return 'isNotArray';
    if (errors.isNotOmogeneous) return 'isNotOmogeneous';
    if (errors.isNotAllNumbers) return 'isNotAllNumbers';
  }

  public jsonSample() {
    return '[{"x": 1, "y": -1}]';
  }

}
