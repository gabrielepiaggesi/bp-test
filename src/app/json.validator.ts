import { FormGroup, FormControl, Validators, AbstractControl, ValidationErrors, ValidatorFn } from '@angular/forms';

export function jsonValidator(): ValidatorFn | null {
    return (control: AbstractControl): ValidationErrors | null => {
        let errors = {
            isNotArray: false,
            isNotJson: false, 
            isNotOmogeneous: false, 
            isNotAllNumbers: false
        };
        let value = control.value.trim();
        
        const jsonResult = isJson(value);
        errors.isNotJson = !jsonResult.isRight;
        if (errors.isNotJson) return errors;
        value = jsonResult.json;

        errors.isNotArray = !isArray(value)
        if (errors.isNotArray) return errors;

        errors.isNotOmogeneous = !isOmogeneous(value);
        if (errors.isNotOmogeneous) return errors;

        errors.isNotAllNumbers = !isWithAllNumbers(value);
        if (errors.isNotAllNumbers) return errors;

        return null;
    }
}

function isArray(obj) { return Array.isArray(obj); };

function isJson(obj): { json: any, isRight: boolean } {
    try {
        return { json: JSON.parse(obj), isRight: true };
    } catch(e) {
        return { json: null, isRight: false };
    }
}

function isOmogeneous(obj): boolean {
    try {
        const allFields = (obj.flatMap(Object.keys));
        const allTheSame: boolean = obj.every(e => {
            return Object.keys(e).length == 2 && allFields.every(field => (field in e))
        });
        return allTheSame;
    } catch(e) {
        return false;
    }
}

function isWithAllNumbers(obj): boolean {
    try {
        const allValues = (obj.flatMap(Object.values));
        const allNumbers = allValues.every(e => {
            return typeof e === 'number' && !isNaN(Math.round(e * 100)/100)
        });
        return allNumbers
    } catch(e) {
        return false;
    }
}