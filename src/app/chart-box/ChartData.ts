export interface ChartData {
    x: string,
    y: string,
    data: {
        name: string,
        series: {}[];
    }[]
}