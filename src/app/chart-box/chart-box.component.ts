import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { DataService } from '../data.service';
import { Subscription } from 'rxjs';
import { ChartData } from './ChartData';

@Component({
  selector: 'app-chart-box',
  templateUrl: './chart-box.component.html',
  styleUrls: ['./chart-box.component.scss']
})
export class ChartBoxComponent implements OnInit {
  public chartDataListener: Subscription;
  public data;
  // chart options
  view: any[] = [800, 400];
  showXAxisLabel: boolean = true;
  showYAxisLabel: boolean = true;
  yAxisLabel: string = '';
  xAxisLabel: string = '';
  colorScheme = {
    domain: ['#5AA454', '#E44D25', '#CFC0BB', '#7aa3e5', '#a8385d', '#aae3f5']
  };

  constructor(private dataService: DataService) { }

  ngOnInit(): void {
    this.chartDataListener = this.dataService.getChartData().subscribe((data: ChartData) => this.loadData(data));
  }

  public loadData(data: ChartData) {
    this.xAxisLabel = data.x;
    this.yAxisLabel = data.y;
    this.data = data.data;
  }

}
