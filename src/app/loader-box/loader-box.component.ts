import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';
import { Subscription } from 'rxjs';
import { ChartData } from '../chart-box/ChartData';

@Component({
  selector: 'app-loader-box',
  templateUrl: './loader-box.component.html',
  styleUrls: ['./loader-box.component.scss']
})
export class LoaderBoxComponent implements OnInit {
  public coordinates = { x: null, y: null };
  public jsonListener: Subscription;
  public data;

  constructor(private dataService: DataService) { }

  ngOnInit(): void {
    this.jsonListener = this.dataService.getJson().subscribe(data => this.loadData(data));
  }

  public loadData(data) {
    this.data = data;
    const options = Object.keys(data[0]);
    this.coordinates.x = options[0];
    this.coordinates.y = options[1];
  }

  public invertXY() {
    const newX = this.coordinates.y;
    this.coordinates.y = this.coordinates.x;
    this.coordinates.x = newX;
  }

  public buildDataForGraph() {
    const finalData = {
      name: 'DATA',
      series: []
    }
    this.data.map((elem) => {
      finalData.series.push({
        r: 5,
        name: '',
        x: elem[this.coordinates.x],
        y: elem[this.coordinates.y],
      })
    });
    const chartData: ChartData = {
      y: this.coordinates.y,
      x: this.coordinates.x,
      data: [finalData]
    }
    this.dataService.setChartData(chartData);
  }

  public dataIsInValid() {
    return !this.coordinates.x || !this.coordinates.y
  }

}
