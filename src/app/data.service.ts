import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { ChartData } from './chart-box/ChartData';

@Injectable({
  providedIn: 'root'
})
export class DataService {
  public jsonData = new Subject();
  public chartData = new Subject<ChartData>();

  constructor() { }

  public setJson(json) {
    this.jsonData.next(json);
  }

  public getJson() {
    return this.jsonData.asObservable();
  }

  public setChartData(chartData: ChartData) {
    this.chartData.next(chartData);
  }

  public getChartData() {
    return this.chartData.asObservable();
  }

}
